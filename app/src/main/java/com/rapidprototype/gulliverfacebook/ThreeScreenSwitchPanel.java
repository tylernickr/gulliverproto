package com.rapidprototype.gulliverfacebook;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by nick on 7/23/15.
 */
public class ThreeScreenSwitchPanel extends SwitchPanel {
    public static final String TAG = "ThreeScreenSwitchPanel";
    public static final int NUMTABS = 3;

    public ThreeScreenSwitchPanel(Context context) {
        this(context, null);
    }

    public ThreeScreenSwitchPanel(Context context, AttributeSet attrs) {
        super(context, attrs, NUMTABS);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float tabWidth = getTabWidth();
        float height = getHeight();

        // draw the three icons
        for (int i = 0; i < NUMTABS; i++) {
            if (getActiveTab() == i) {
                canvas.drawCircle(tabWidth * i + tabWidth / 2, height / 2, height / 4, getPrimaryPaint());
            } else {
                canvas.drawCircle(tabWidth * i + tabWidth / 2, height / 2, height / 4, getAccentPaint());
            }
        }
    }
}
