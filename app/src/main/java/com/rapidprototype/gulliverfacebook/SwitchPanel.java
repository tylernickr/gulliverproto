package com.rapidprototype.gulliverfacebook;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by nick on 8/2/15.
 */
abstract class SwitchPanel extends View {
    private int activeTab;
    private int numTabs;

    private Paint accentPaint = new Paint();
    private Paint primaryPaint = new Paint();

    public SwitchPanel(Context context, int numPanels) {
        this(context, null, numPanels);
    }

    public SwitchPanel(Context context, AttributeSet attrs, int numTabs) {
        super(context, attrs);
        this.numTabs = numTabs;
        accentPaint.setColor(getResources().getColor(R.color.white));
        primaryPaint.setColor(getResources().getColor(R.color.primary_color));
    }

    public void setActiveTab(int activeTab) {
        this.activeTab = activeTab;
    }

    public float getTabWidth() {
        return getWidth() / numTabs;
    }

    public int getActiveTab() {
        return activeTab;
    }

    public Paint getAccentPaint() {
        return accentPaint;
    }

    public void setAccentPaint(Paint accentPaint) {
        this.accentPaint = accentPaint;
    }

    public Paint getPrimaryPaint() {
        return primaryPaint;
    }

    public void setPrimaryPaint(Paint primaryPaint) {
        this.primaryPaint = primaryPaint;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPaint(primaryPaint);
        float height = getHeight();
        float tabWidth = getTabWidth();

        // draw the active tab inverse colors
        canvas.drawRect(tabWidth * activeTab, 0, tabWidth * (activeTab + 1), height, accentPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        int activeTab = getIndexFromTouch(touchEvent.getX());
        setActiveTab(activeTab);
        invalidate();
        return true;
    }

    private int getIndexFromTouch(float xTouch) {
        float tabWidth = getWidth() / 3;
        return (int)Math.floor(xTouch / tabWidth);
    }


}
