package com.rapidprototype.gulliverfacebook;

import android.app.Application;

import com.facebook.FacebookSdk;

/**
 * Created by nick on 7/9/15.
 */
public class GulliverApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
